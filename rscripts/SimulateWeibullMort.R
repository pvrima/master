# DESCRIPTION: Code to simulate data on age-specific mortality with sex and 
#              a continuous covariate to be analyse with BaSTA. The code 
#              simulates mortality using a Weibull mortality function under a
#              proportional hazards framework for the continuous covariate.
# AUTHOR: Pedro Martins - Based on Colchero's simulation with gompertz model
# DATE: 19-jan-2014
# ============================================================================ #
# SETTINGS TO BE MODIFIED ACCORDINGLY TO YOUR INTEREST:
# Parameters to simulate data:
theta <- rbind(c(12, 0.2), c(5, 0.1))
dimnames(theta) <- list(c("f","m"), c("b0","b1"))
gamma <- 0.3
probKnownB <- 0.3
probKnownD <- 0.3
recapProb <- 0.7

# Initial number of individuals:
nini <- 2000

# Min and max times of birth:
minbirth <- 1900
maxbirth <- 1999

# Study duration:
t1 <- 1901
tT <- 2000

# SIMULATION:
# Weibull inverse sampling for random:
rweib <- function(thetata, gammama) {
  u <- 0.5
  rx <- ((-log(1-u))^(1/thetata[,1]))/thetata[,2]+gammama
  
  return(rx)
}

# Simulate sexes (1:1 sex ratio):
sexmat <- matrix(0, nini, 2, dimnames = list(NULL, c("f", "m")))
sexmat[, "f"] <- rbinom(nini, 1, 0.5)
sexmat[, "m"] <- 1 - sexmat[, "f"]

# Simulate continuous covariate:
cont <- rnorm(nini, 0, 0.5)

# Simulate ages at death:
x <- rweib(sexmat %*% theta, gamma * cont)

# Simulate times of birth and death:
b <- sample(minbirth:maxbirth, nini, replace = TRUE)
d <- floor(b + x)

# Simulate capture histories:
tvec <- t1:tT
nt <- length(tvec)
H1 <- matrix(tvec, nini, nt, byrow = TRUE, dimnames = list(NULL, tvec)) - b
H1[H1 <= 0] <- 0; H1[H1 > 0] <- 1
H2 <- d - matrix(tvec, nini, nt, byrow = TRUE, dimnames = list(NULL, tvec))
H2[H2 >= 0] <- 1; H2[H2 < 0] <- 0
H <- H1 * H2
Hobs <- H * matrix(rbinom(length(H), 1, recapProb), nini, nt)
nobs <- Hobs %*% rep(1, nt)

# simulate observed times of birth and death:
bobs <- b * rbinom(nini, 1, probKnownB)
dobs <- d * rbinom(nini, 1, probKnownD)

# build final dataset (dat):
idseen <- which(d >= t1 & b <= tT & (nobs > 0 | bobs > 0 | dobs > 0))
ID <- 1:nini
dat <- data.frame(ID, bobs, dobs, Hobs, sexmat, cont)[idseen, ]

# END OF CODE.
out <- basta(object = dat, studyStart = t1, studyEnd = tT,
            model = "WE",nsim=2,ncpus=2,parallel=T)


